﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADTool.Data;
using ADTool.Data.DapperClient;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Resource;

namespace ADWebWindowsServices.Query
{
    internal class ADWebWindowsServicesQuery
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// object to storage the object for access to database
        /// </summary>
        private DapperModel DBModel = null;
        /// <summary>
        /// object to manage the access to database
        /// </summary>
        private DapperManager DBManager = null;
        /// <summary>
        /// 
        /// </summary>
        private string connectionString = String.Empty;
        /// <summary>
        /// 
        /// </summary>
        private ADWebWindowsServicesEnvironmentQuery EnvironmentQuery = null;
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesQuery()
        {
        }
        #endregion

        #region - M E T H  O D S

        /// <summary>
        /// 
        /// </summary>
        private void InitializeInstances()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebWindowsServicesEnvironmentQuery();
            if (String.IsNullOrEmpty(connectionString)) connectionString = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyser, DataConnectionTypeProviderEnum.MSSQL);
            if (DBManager == null) DBManager = new DapperManager();
            if (DBModel == null) DBModel = new DapperModel(connectionString, DataConnectionTypeProviderEnum.MSSQL);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool AreInstancesLoaded()
        {
            return (EnvironmentQuery != null) && (!String.IsNullOrEmpty(connectionString)) && (DBManager != null) && (DBModel != null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameServices"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public int GetServicesId(string nameServices)
        {
            int result = default(int);
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdServices] FROM {EnvironmentQuery.GetActivEnvironment().ServicesTable} WHERE [Name] = @Name;";
            result = DBManager.QueryFirstOrDefault<int>(DBModel, query, new { Name = nameServices });
            if (result == default(int)) throw new Exception($"The name '{nameServices}' it's not references to any services installed.");
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameServices"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebWindowsServicesEntity GetServices(string nameServices)
        {
            ADWebWindowsServicesEntity result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdServices],[Name],[InstalledDate],[IntervalExecutionTimeMiliSecond] 
                                FROM {EnvironmentQuery.GetActivEnvironment().ServicesTable} WHERE [Name] = @Name;";
            result = DBManager.QueryFirstOrDefault<ADWebWindowsServicesEntity>(DBModel, query, new { Name = nameServices });
            if (result == null) throw new Exception($"The name '{nameServices}' it's not references to any services installed.");
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Services"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesProcessEntity> GetProcess(ADWebWindowsServicesEntity Services)
        {
            IEnumerable<ADWebWindowsServicesProcessEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdProcess], [IdServices], [Name], [InitialStart], [LastStart], [IsProcessRunning], [ScheduleTime] 
                                FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessTable} WHERE [IdServices] = @IdServices;";
            result = DBManager.Query<ADWebWindowsServicesProcessEntity>(DBModel, query, Services);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="idProcess"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebWindowsServicesProcessEntity GetProcess(ADWebWindowsServicesEntity Services, int idProcess)
        {
            ADWebWindowsServicesProcessEntity result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdProcess], [IdServices], [Name], [InitialStart], [LastStart], [IsProcessRunning], [ScheduleTime] 
                            FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessTable} WHERE [IdServices] = @IdServices and [IdProcess] = @IdProcess;";
            result = DBManager.QueryFirstOrDefault<ADWebWindowsServicesProcessEntity>(DBModel, query, new { IdServices = Services.IdServices, IdProcess = idProcess });
            if (result == null) throw new Exception($"The process with the id '{idProcess}' and with the services named '{Services.Name}' aren't references to any process installed.");
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idServices"></param>
        /// <param name="idProcess"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebWindowsServicesProcessEntity GetProcess(int idServices, int idProcess)
        {
            ADWebWindowsServicesProcessEntity result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdProcess], [IdServices], [Name], [InitialStart], [LastStart], [IsProcessRunning], [ScheduleTime] 
                            FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessTable} WHERE [IdServices] = @IdServices and [IdProcess] = @IdProcess;";
            result = DBManager.QueryFirstOrDefault<ADWebWindowsServicesProcessEntity>(DBModel, query, new { IdServices = idServices, IdProcess = idProcess });
            if (result == null) throw new Exception($"The process with the id '{idProcess}' and id services '{idServices}' aren't references to any process installed.");
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesProcessTaskEntity> GetTask(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesProcessTaskEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdTask], [IdProcess], [Name], [LastStart], [ActiveTask] 
                                FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessTaskTable} WHERE [IdProcess] = @IdProcess;";
            result = DBManager.Query<ADWebWindowsServicesProcessTaskEntity>(DBModel, query, Process);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idProcess"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessTaskEntity> GetTask(int idProcess)
        {
            IEnumerable<ADWebWindowsServicesProcessTaskEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdTask], [IdProcess], [Name], [LastStart], [ActiveTask] 
                                FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessTaskTable} WHERE [IdProcess] = @IdProcess;";
            result = DBManager.Query<ADWebWindowsServicesProcessTaskEntity>(DBModel, query, new { IdProcess = idProcess });
            return result;
        }
        /// <summary>
        /// Get all row info about execution process 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesProcessExecutionLogEntity> GetExectionProcessLog()
        {
            IEnumerable<ADWebWindowsServicesProcessExecutionLogEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdExecutionLog], [IdProcess], [ScheduleTime], [Date] FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessExecutionLog};";
            result = DBManager.Query<ADWebWindowsServicesProcessExecutionLogEntity>(DBModel, query);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesProcessExecutionLogEntity> GetExectionProcessLog(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesProcessExecutionLogEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdExecutionLog], [IdProcess], [ScheduleTime], [Date] FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessExecutionLog} WHERE [IdProcess] = @IdProcess;";
            result = DBManager.Query<ADWebWindowsServicesProcessExecutionLogEntity>(DBModel, query, Process);
            return result;
        }
        /// <summary>
        /// Save the process execution log in database
        /// </summary>
        /// <param name="ProcessExecutionLog"></param>
        /// <exception cref="Exception"></exception>
        public void SaveExecutionProcessLog(ADWebWindowsServicesProcessExecutionLogEntity ProcessExecutionLog)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"INSERT INTO {EnvironmentQuery.GetActivEnvironment().ServicesProcessExecutionLog}([IdProcess],[ScheduleTime],[Date]) 
                            VALUES(@IdProcess, @ScheduleTime, @Date);";
            DBManager.Insert(DBModel, query, ProcessExecutionLog);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesConfigDataEntity> GetProcessConfigData(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesConfigDataEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdProcess] as [IdEntity], [Key], [Value] 
                                FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessConfigTable} WHERE [IdProcess] = @IdProces;";
            result = DBManager.Query<ADWebWindowsServicesConfigDataEntity>(DBModel, query, Process);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesConfigDataEntity> GetTaskConfigData(ADWebWindowsServicesProcessTaskEntity Task)
        {
            IEnumerable<ADWebWindowsServicesConfigDataEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"SELECT [IdTask] as [IdEntity], [Key], [Value] FROM {EnvironmentQuery.GetActivEnvironment().ServicesProcessTaskConfigTable} 
	                            WHERE [IdTask] = @IdTask;";
            result = DBManager.Query<ADWebWindowsServicesConfigDataEntity>(DBModel, query, Task);
            return result;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <exception cref="Exception"></exception>
        public void SaveStatusProcess(ADWebWindowsServicesProcessEntity Process)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"UPDATE {EnvironmentQuery.GetActivEnvironment().ServicesProcessTable}
                                SET [Name] = @Name, [InitialStart] = @InitialStart, [LastStart] = @LastStart, [IsProcessRunning] = @IsProcessRunning, [ScheduleTime] = @ScheduleTime
                                WHERE [IdProcess] = @IdProcess AND [IdServices] = @IdServices;";
            DBManager.Update(DBModel, query, Process);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <exception cref="Exception"></exception>
        public void SaveStatusTask(ADWebWindowsServicesProcessTaskEntity Task)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"UPDATE {EnvironmentQuery.GetActivEnvironment().ServicesProcessTaskTable}
                                SET [Name] = @Name, [LastStart] = @LastStart, [ActiveTask] = @ActiveTask
                                WHERE [IdTask] = @IdTask AND [IdProcess] = @IdProcess;";
            DBManager.Update(DBModel, query, Task);
        }
        /// <summary>
        /// 
        /// </summary>
        public bool ThereIsConnectionWithDatabaseEngineActive()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            return DBManager.ThereIsConectivity(DBModel);
        }
        /// <summary>
        /// 
        /// </summary>
        public void CreateGeneralServicesTables()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebWindowsServicesCreateGeneralTablesSQL);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        public void DropGeneralServicesTables()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = ADTool.CustomFile.FileManager.GetFileFromAppPath(EnvironmentQuery.GetActivEnvironment().ADWebWindowsServicesDropGeneralTablesSQL);
            DBManager.Execute(DBModel, query);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ThereIsSomeServicesInstalled()
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $"SELECT COUNT(*) FROM {EnvironmentQuery.GetActivEnvironment().ServicesTable}";
            return DBManager.QueryFirstOrDefault<bool>(DBModel, query);
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebWindowsServicesQuery Destructor
        /// </summary>
        ~ADWebWindowsServicesQuery()
        {
            Dispose(false);
        }

        /// <summary>
        /// ADWebWindowsServicesQuery Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (DBManager != null) DBManager.Dispose();
                    if (DBModel != null) DBModel.Dispose();
                    DBManager = null;
                    DBModel = null;
                    EnvironmentQuery = null;
                    connectionString = String.Empty;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion
    }
}
