﻿using ADTool.Environment;
using ADTool.CustomFile;
using ADTool.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ADWebWindowsServices.Entity;

namespace ADWebWindowsServices.Query
{
    internal class ADWebWindowsServicesEnvironmentQuery
    {
        /// <summary>
        /// Private environment manager.
        /// </summary>
        private EnvironmentManager<ADWebWindowsServicesEnvironmentEntity> envManager = null;
        /// <summary>
        /// Environment file name.
        /// </summary>
        private const string envConfigFileName = "ConfigFile\\ADWebWindowsServicesEnv.cfg";

        /// <summary>
        /// Constructor.
        /// </summary>
        public ADWebWindowsServicesEnvironmentQuery()
        {
            envManager = new EnvironmentManager<ADWebWindowsServicesEnvironmentEntity>();
        }

        /// <summary>
        /// Load the environment if it's not loaded yet.
        /// </summary>
        /// <exception cref="Exception"></exception>
        private void LoadEnvironmentData()
        {
            if (!envManager.IsEnvironmentLoaded())
            {
                string pathEnvConfig = Path.Combine(FileManager.GetApplicationPath(), envConfigFileName);
                envManager.LoadEnvironment(pathEnvConfig);
            }
        }
        /// <summary>
        /// Get the connection string.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public string GetConnectionString(string connectionName, DataConnectionTypeProviderEnum typeProviderEnum)
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment().Connections.Where(conn => conn.ConnectionName == connectionName).First().GetConnectionString(typeProviderEnum);
        }
        /// <summary>
        /// Get the connection object.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public DataConnection GetConnection(string connectionName)
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment().Connections.Where(conn => conn.ConnectionName == connectionName).First();
        }
        /// <summary>
        /// Get the active environment.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebWindowsServicesEnvironmentEntity GetActivEnvironment()
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment();
        }
    }
}
