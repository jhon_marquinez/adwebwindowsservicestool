﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADTool.Data.DapperClient;
using ADWebWindowsServices.Resource;
using ADWebWindowsServices.Enum;
using ADTool.Data;
using System.Data.SqlClient;

namespace ADWebWindowsServices.Query
{
    internal class ADWebWindowsServicesLogQuery: IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// object to storage the object for access to database
        /// </summary>
        private DapperModel DBModel = null;
        /// <summary>
        /// object to manage the access to database
        /// </summary>
        private DapperManager DBManager = null;
        /// <summary>
        /// 
        /// </summary>
        private string connectionString = String.Empty;
        /// <summary>
        /// 
        /// </summary>
        private ADWebWindowsServicesEnvironmentQuery EnvironmentQuery = null;
        #endregion


        public ADWebWindowsServicesLogQuery()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        private void InitializeInstances()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebWindowsServicesEnvironmentQuery();
            if (String.IsNullOrEmpty(connectionString)) connectionString = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyser, DataConnectionTypeProviderEnum.MSSQL);
            if (DBManager == null) DBManager = new DapperManager();
            if (DBModel == null) DBModel = new DapperModel(connectionString, DataConnectionTypeProviderEnum.MSSQL);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool AreInstancesLoaded()
        {
            return (EnvironmentQuery != null) && (!String.IsNullOrEmpty(connectionString)) && (DBManager != null) && (DBModel != null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServicesEntity"></param>
        public void SaveServicesLog(IADWebWindowsServicesLogEntity ServicesEntity)
        {
            SaveServicesLog(new List<IADWebWindowsServicesLogEntity> { ServicesEntity });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServicesLogEntity"></param>
        public void SaveServicesLog(IEnumerable<IADWebWindowsServicesLogEntity> ServicesLogEntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"INSERT INTO {EnvironmentQuery.GetActivEnvironment().ServicesTableLog}([IdServices],[Status],[ExceptionJson],[TypeLog],[CreatedAt]) 
                            VALUES(@IdEntity, @Status, @ExceptionJson, @TypeLog, @CreatedAt);";
            foreach (ADWebWindowsServicesLogEntity ServicesLog in ServicesLogEntity)
            {
                DBManager.Insert(DBModel, query, ServicesLogEntity);
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServicesProcessEntity"></param>
        public void SaveServicesProcessLog(IADWebWindowsServicesLogEntity ServicesProcessEntity)
        {
            SaveServicesProcessLog(new List<IADWebWindowsServicesLogEntity> { ServicesProcessEntity });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServicesProcessLogEntity"></param>
        public void SaveServicesProcessLog(IEnumerable<IADWebWindowsServicesLogEntity> ServicesProcessLogEntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"INSERT INTO {EnvironmentQuery.GetActivEnvironment().ServicesProcessTableLog}([IdProcess],[TypeLog],[Message],[ExceptionJson],[CreatedAt]) 
                            VALUES(@IdEntity, @TypeLog, @Message, @ExceptionJson, @CreatedAt);";
            foreach (ADWebWindowsServicesProcessLogEntity ServicesProcessLog in ServicesProcessLogEntity)
            {
                DBManager.Insert(DBModel, query, ServicesProcessLog);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServicesProcessTaskEntity"></param>
        public void SaveServicesProcessTaskLog(IADWebWindowsServicesLogEntity ServicesProcessTaskEntity)
        {
            SaveServicesProcessTaskLog(new List<IADWebWindowsServicesLogEntity> { ServicesProcessTaskEntity });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServicesProcessTaskLogEntity"></param>
        public void SaveServicesProcessTaskLog(IEnumerable<IADWebWindowsServicesLogEntity> ServicesProcessTaskLogEntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"INSERT INTO {EnvironmentQuery.GetActivEnvironment().ServicesProcessTaskTableLog}([IdTask],[TypeLog],[Message],[ExceptionJson],[CreatedAt]) 
                            VALUES(@IdEntity, @TypeLog, @Message, @ExceptionJson, @CreatedAt);";
            foreach (ADWebWindowsServicesProcessTaskLogEntity ServicesProcessLog in ServicesProcessTaskLogEntity)
            {
                DBManager.Insert(DBModel, query, ServicesProcessLog);
            }
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesLogQuery Destructor
        /// </summary>
        ~ADWebWindowsServicesLogQuery()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if(DBManager != null) DBManager.Dispose();
                    if(DBModel != null) DBModel.Dispose();
                    DBManager = null;
                    DBModel = null;
                    EnvironmentQuery = null;
                    connectionString = String.Empty;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

    }
}
