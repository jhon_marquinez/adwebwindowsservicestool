﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADTool.Log;

namespace ADWebWindowsServices.Entity
{
    internal class ADWebWindowsServicesProcessTaskLogEntity: IADWebWindowsServicesLogEntity
    {
        public int? IdLog { get; set; }
        public int IdEntity{ get; set; }
        public ADTool.Log.LogType TypeLog { get; set; }
        public string Message { get; set; }
        public string ExceptionJson { get; set; }
        public DateTime CreatedAt { get; set; }

        public ADWebWindowsServicesProcessTaskLogEntity()
        {
        }

        public ADWebWindowsServicesProcessTaskLogEntity(int idEntity, LogType typeLog, string message, string exceptionJson, DateTime createdAt, int? idLog = null)
        {
            IdLog = idLog;
            IdEntity = idEntity;
            TypeLog = typeLog;
            Message = message;
            ExceptionJson = exceptionJson;
            CreatedAt = createdAt;
        }
    }
}
