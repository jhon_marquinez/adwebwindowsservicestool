﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    public class ADWebWindowsServicesProcessTaskEntity
    {
        public int IdTask { get; set; }
        public int IdProcess { get; set; }
        public string Name { get; set; }
        public DateTime? LastStart { get; set; }
        public bool ActiveTask { get; set; }

        public ADWebWindowsServicesProcessTaskEntity()
        {
        }

        public ADWebWindowsServicesProcessTaskEntity(int idTask, int idProcess, string name, DateTime? lastStart, bool activeTask)
        {
            IdTask = idTask;
            IdProcess = idProcess;
            Name = name;
            LastStart = lastStart;
            ActiveTask = activeTask;
        }
    }
}
