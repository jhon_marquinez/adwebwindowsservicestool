﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    public class ADWebWindowsServicesProcessExecutionLogEntity
    {
        public int? IdExecutionLog { get; set; }
        public int IdProcess { get; set; }
        public TimeSpan ScheduleTime { get; set; }
        public DateTime Date { get; set; }

        public ADWebWindowsServicesProcessExecutionLogEntity()
        {
        }

        public ADWebWindowsServicesProcessExecutionLogEntity(int idProcess, TimeSpan scheduleTime, DateTime date)
        {
            IdExecutionLog = null;
            IdProcess = idProcess;
            ScheduleTime = scheduleTime;
            Date = date;
        }

        public ADWebWindowsServicesProcessExecutionLogEntity(int? idExecutionLog, int idProcess, TimeSpan scheduleTime, DateTime date)
        {
            this.IdExecutionLog = idExecutionLog;
            IdProcess = idProcess;
            ScheduleTime = scheduleTime;
            Date = date;
        }
    }
}
