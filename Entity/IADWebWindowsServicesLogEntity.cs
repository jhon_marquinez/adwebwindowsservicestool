﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    internal interface IADWebWindowsServicesLogEntity
    {
        int? IdLog { get; set; }
        int IdEntity { get; set; }
        ADTool.Log.LogType TypeLog { get; set; }
        string ExceptionJson { get; set; }
        DateTime CreatedAt { get; set; }
    }
}
