﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    public class ADWebWindowsServicesConfigDataEntity
    {
        public int IdEntity { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        public ADWebWindowsServicesConfigDataEntity()
        {
        }

        public ADWebWindowsServicesConfigDataEntity(int idEntity, string key, string value)
        {
            IdEntity = idEntity;
            Key = key;
            Value = value;
        }
    }
}
