﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    public class ADWebWindowsServicesEntity
    {
        public int IdServices { get; set; }
        public string Name { get; set; }
        public DateTime? InstalledDate { get; set; }
        public int IntervalExecutionTimeMiliSecond { get; set; }

        public ADWebWindowsServicesEntity()
        {
        }

        public ADWebWindowsServicesEntity(int idServices, string name, DateTime? installedDate, int intervalExecutionTimeMiliSecond)
        {
            IdServices = idServices;
            Name = name;
            InstalledDate = installedDate;
            IntervalExecutionTimeMiliSecond = intervalExecutionTimeMiliSecond;
        }
    }
}
