﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class ADWebWindowsServicesException
    {
        /// <summary>
        /// 
        /// </summary>
        public string TargetSite { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Source { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DeclaringType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MemberType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesException()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetSite"></param>
        /// <param name="message"></param>
        /// <param name="source"></param>
        /// <param name="declaringType"></param>
        /// <param name="memberType"></param>
        /// <param name="stackTrace"></param>
        public ADWebWindowsServicesException(string targetSite, string message, string source, string declaringType, string memberType, string stackTrace)
        {
            TargetSite = targetSite;
            Message = message;
            Source = source;
            DeclaringType = declaringType;
            MemberType = memberType;
            StackTrace = stackTrace;
        }
    }
}
