﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADTool.Log;
using ADWebWindowsServices.Enum;

namespace ADWebWindowsServices.Entity
{
    internal class ADWebWindowsServicesLogEntity: IADWebWindowsServicesLogEntity
    {
        public int? IdLog { get; set; }
        public int IdEntity{ get; set; }
        public ADWebWindowsServicesStatus Status { get; set; }
        public string ExceptionJson { get; set; }
        public ADTool.Log.LogType TypeLog { get; set; }
        public DateTime CreatedAt { get; set; }

        public ADWebWindowsServicesLogEntity()
        {
        }

        public ADWebWindowsServicesLogEntity(int idEntity, ADWebWindowsServicesStatus status, string exceptionJson, LogType typeLog, DateTime createdAt, int? idLog = null)
        {
            IdLog = idLog;
            IdEntity = idEntity;
            Status = status;
            ExceptionJson = exceptionJson;
            TypeLog = typeLog;
            CreatedAt = createdAt;
        }
    }
}
