﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebWindowsServices.Entity
{
    public class ADWebWindowsServicesProcessEntity
    {
        public int IdProcess { get; set; }
        public int IdServices { get; set; }
        public string Name { get; set; }
        public DateTime? InitialStart { get; set; }
        public DateTime? LastStart { get; set; }
        public bool IsProcessRunning { get; set; }
        public string ScheduleTime { get; set; }

        public ADWebWindowsServicesProcessEntity()
        {
        }

        public ADWebWindowsServicesProcessEntity(int idProcess, int idServices, string name, DateTime? initialStart, DateTime? lastStart, bool isProcessRunning, string scheduleTime)
        {
            IdProcess = idProcess;
            IdServices = idServices;
            Name = name;
            InitialStart = initialStart;
            LastStart = lastStart;
            IsProcessRunning = isProcessRunning;
            ScheduleTime = scheduleTime;
        }
    }

}
