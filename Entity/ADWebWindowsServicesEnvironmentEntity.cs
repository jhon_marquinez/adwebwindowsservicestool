﻿using ADTool.Environment;
using ADTool.Data;
using System.Collections.Generic;

namespace ADWebWindowsServices.Entity
{
    internal class ADWebWindowsServicesEnvironmentEntity : IEnvironment
    {
        public string EnvironmentName { get; set; }
        public bool ActiveEnvironment { get; set; }
        public DataEnvironmentTypeEnum DataEnvironmentType { get; set; }
        public IEnumerable<DataConnection> Connections { get; set; }
        public string ServicesTable { get; set; }
        public string ServicesTableLog { get; set; }
        public string ServicesProcessTableLog { get; set; }
        public string ServicesProcessTaskTableLog { get; set; }
        public string ServicesProcessTable { get; set; }
        public string ServicesProcessTaskTable { get; set; }
        public string ServicesProcessExecutionLog { get; set; }
        public string ServicesProcessConfigTable { get; set; }
        public string ServicesProcessTaskConfigTable { get; set; }
        public string ADWebWindowsServicesCreateGeneralTablesSQL { get; set; }
        public string ADWebWindowsServicesDropGeneralTablesSQL { get; set; }
    }
}
