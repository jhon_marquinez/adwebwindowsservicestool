﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Repository;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Enum;

namespace ADWebWindowsServices.Manager
{
    internal class ADWebWindowsServicesLogManager
    {
        /// <summary>
        /// 
        /// </summary>
        private ADWebWindowsServicesLogRepository  ServicesRepositoryLog;

        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesLogManager()
        {
            ServicesRepositoryLog = new ADWebWindowsServicesLogRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TypeEntity"></param>
        /// <param name="LogEntity"></param>
        /// <exception cref="Exception"></exception>
        public void AddLog<T>(ADWebWindowsServicesTypeEntity TypeEntity, T LogEntity) 
            where T : IADWebWindowsServicesLogEntity
        {
            ServicesRepositoryLog.AddLog<T>(TypeEntity, LogEntity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TypeEntity"></param>
        /// <param name="LogEntity"></param>
        /// /// <exception cref="Exception"></exception>
        public void AddListLog<T>(ADWebWindowsServicesTypeEntity TypeEntity, T LogEntity)
            where T : IEnumerable<IADWebWindowsServicesLogEntity>
        {
            ServicesRepositoryLog.AddListLog<T>(TypeEntity, LogEntity);
        }

    }
}
