﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Repository;
using ADTool.Log;
using ADWebWindowsServices.Enum;
using ADWebWindowsServices.Resource;

namespace ADWebWindowsServices.Manager
{
    public class ADWebWindowsServicesManager
    {
        /// <summary>
        /// 
        /// </summary>
        private ADWebWindowsServicesLogManager ServicesLogManager;

        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesManager()
        {
            ServicesLogManager = new ADWebWindowsServicesLogManager();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public int GetServicesId(string serviceName)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetServicesId(serviceName);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebWindowsServicesEntity GetServices(string serviceName)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetServices(serviceName);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Services"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessEntity> GetProcessRunning(ADWebWindowsServicesEntity Services)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetProcessRunning(Services);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Services"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessEntity> GetProcessNotRunning(ADWebWindowsServicesEntity Services)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetProcessNotRunning(Services);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessTaskEntity> GetTaskActive(ADWebWindowsServicesProcessEntity Process)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetTaskActive(Process);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessTaskEntity> GetTaskDesactive(ADWebWindowsServicesProcessEntity Process)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetTaskDesactive(Process);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesConfigDataEntity> GetProcessConfigData(ADWebWindowsServicesProcessEntity Process)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetProcessConfigData(Process);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="keyConfig"></param>
        /// <returns></returns>
        public string GetProcessConfigData(ADWebWindowsServicesProcessEntity Process, string keyConfig)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetProcessConfigData(Process, keyConfig);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesConfigDataEntity> GetTaskConfigData(ADWebWindowsServicesProcessTaskEntity Task)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetTaskConfigData(Task);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <param name="keyConfig"></param>
        /// <returns></returns>
        public string GetTaskConfigData(ADWebWindowsServicesProcessTaskEntity Task, string keyConfig)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.GetTaskConfigData(Task, keyConfig);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        public void StartProcess(ADWebWindowsServicesProcessEntity Process)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.StartProcess(Process);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        public void StartTask(ADWebWindowsServicesProcessTaskEntity Task)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.StartTask(Task);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        public void StopProcess(ADWebWindowsServicesProcessEntity Process)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.StopProcess(Process);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ProcessRunning"></param>
        /// <exception cref="Exception"></exception>
        public void AddLogProcessRunning(IEnumerable<ADWebWindowsServicesProcessEntity> ProcessRunning)
        {
            List<ADWebWindowsServicesProcessLogEntity> logList = new List<ADWebWindowsServicesProcessLogEntity>();
            foreach (ADWebWindowsServicesProcessEntity ServicesProcess in ProcessRunning)
            {
                logList.Add(
                    new ADWebWindowsServicesProcessLogEntity(
                        ServicesProcess.IdProcess,
                        LogType.Warning,
                        $"The process {ServicesProcess.Name} can't execute because it's running now.",
                        String.Empty,
                        DateTime.Now
                    )
                );
            }
            ServicesLogManager.AddListLog<IEnumerable<ADWebWindowsServicesProcessLogEntity>>(ADWebWindowsServicesTypeEntity.Process, logList.AsEnumerable());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Services"></param>
        /// <param name="status"></param>
        /// <param name=""></param>
        /// <param name="logType"></param>
        /// <param name="jsonException"></param>
        /// <param name=""></param>
        public void AddLog(ADWebWindowsServicesEntity Services, ADWebWindowsServicesStatus status, LogType logType = LogType.Success, string jsonException = null)
        {
            ServicesLogManager
                .AddLog<ADWebWindowsServicesLogEntity>(
                    ADWebWindowsServicesTypeEntity.Services,
                    new ADWebWindowsServicesLogEntity(
                        Services.IdServices,
                        status,
                        jsonException ?? String.Empty,
                        logType,
                        DateTime.Now
                    )
                );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="messageLog"></param>
        /// <param name="logType"></param>
        /// <param name="jsonException"></param>
        /// <exception cref="Exception"></exception>
        public void AddLog(ADWebWindowsServicesProcessEntity Process, string messageLog, LogType logType = LogType.Success, string jsonException = null)
        {
            ServicesLogManager
                .AddLog<ADWebWindowsServicesProcessLogEntity>(
                    ADWebWindowsServicesTypeEntity.Process,
                    new ADWebWindowsServicesProcessLogEntity(
                        Process.IdProcess,
                        logType,
                        messageLog,
                        jsonException ?? String.Empty,
                        DateTime.Now
                    )
                );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <param name="messageLog"></param>
        /// <param name="logType"></param>
        /// <param name="jsonException"></param>
        /// <exception cref="Exception"></exception>
        public void AddLog(ADWebWindowsServicesProcessTaskEntity Task, string messageLog, LogType logType = LogType.Success, string jsonException = null)
        {
            ServicesLogManager
                .AddLog<ADWebWindowsServicesProcessTaskLogEntity>(
                    ADWebWindowsServicesTypeEntity.Task,
                    new ADWebWindowsServicesProcessTaskLogEntity(
                        Task.IdProcess,
                        logType,
                        messageLog,
                        jsonException ?? String.Empty,
                        DateTime.Now
                    )
                );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="ScheduleTime"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool ProcessHasbeenExecuted(ADWebWindowsServicesProcessEntity Process, TimeSpan ScheduleTime, DateTime Date)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.CheckIfProcessHasbeenExecuted(Process, ScheduleTime, Date);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool IsAllowExecutionThisProcessAfterCheckScheduleTime(ADWebWindowsServicesProcessEntity Process)
        {
            if (String.IsNullOrEmpty(Process.ScheduleTime)) return true;
            bool isAllow = false;
            DateTime CurrentDateTime = DateTime.Now;
            IOrderedEnumerable<string> scheduletimesExecutionProcess =
                Process.ScheduleTime.Split(ADWebWindowsServicesConstantValue.SeparatorScheduleTimeProcessExecution)
                .OrderBy(time => Convert.ToInt32(time.Split(ADWebWindowsServicesConstantValue.SeparatorScheduleHoreInTimeProcessExecution)[0]));
            foreach (string scheduleTime in scheduletimesExecutionProcess)
            {
                TimeSpan ScheduleTimeFormatted = DateTime.Parse(scheduleTime).TimeOfDay;
                if (CurrentDateTime.TimeOfDay >= ScheduleTimeFormatted)
                    if (!ProcessHasbeenExecuted(Process, ScheduleTimeFormatted, CurrentDateTime.Date))
                    {
                        AddExecutionProcessLog(Process, ScheduleTimeFormatted, CurrentDateTime.Date);
                        isAllow = true;
                        break;
                    }
            }
            return isAllow;
        }
        /// <summary>
        /// Save log about process execution
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="ScheduleTime"></param>
        /// <param name="Date"></param>
        /// <exception cref="Exception"></exception>
        public void AddExecutionProcessLog(ADWebWindowsServicesProcessEntity Process, TimeSpan ScheduleTime, DateTime Date)
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.AddExecutionProcessLog(Process, ScheduleTime, Date);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ADWebWindowsServicesManager CheckConnetivityWithDatabaseEngine()
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.CheckConnetivityWithDatabaseEngine();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesManager CreateGeneralServicesTables()
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.CreateGeneralServicesTables();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesManager DropGeneralServicesTables()
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                ServicesRepository.DropGeneralServicesTables();
            }
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ThereIsSomeServicesInstalled()
        {
            using (ADWebWindowsServicesRepository ServicesRepository = new ADWebWindowsServicesRepository())
            {
                return ServicesRepository.ThereIsSomeServicesInstalled();
            }
        }
    }
}
