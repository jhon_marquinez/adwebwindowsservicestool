﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Query;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Enum;

namespace ADWebWindowsServices.Repository
{
    internal class ADWebWindowsServicesLogRepository
    {
        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesLogRepository()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TypeEntity"></param>
        /// <param name="LogEntity"></param>
        /// <exception cref="Exception"></exception>
        public void AddLog<T>(ADWebWindowsServicesTypeEntity TypeEntity, T LogEntity) 
            where T: IADWebWindowsServicesLogEntity
        {
            using (ADWebWindowsServicesLogQuery ServicesQueryLog = new ADWebWindowsServicesLogQuery())
            {
                switch (TypeEntity)
                {
                    case ADWebWindowsServicesTypeEntity.Services:
                        if (LogEntity is ADWebWindowsServicesLogEntity)
                        {
                            ServicesQueryLog.SaveServicesLog(LogEntity);
                        }
                        else
                        {
                            throw new Exception("The log entity is not the ADWebWindowsServicesLogEntity class object."); }
                        break;
                    case ADWebWindowsServicesTypeEntity.Process:
                        if (LogEntity is ADWebWindowsServicesProcessLogEntity)
                        {
                            ServicesQueryLog.SaveServicesProcessLog(LogEntity);
                        }
                        else
                        { throw new Exception("The log entity is not the ADWebWindowsServicesProcessLogEntity class object."); }
                        break;
                    case ADWebWindowsServicesTypeEntity.Task:
                        if (LogEntity is ADWebWindowsServicesProcessTaskLogEntity)
                        {
                            ServicesQueryLog.SaveServicesProcessTaskLog(LogEntity);
                        }
                        else
                        { throw new Exception("The log entity is not the ADWebWindowsServicesProcessTaskLogEntity class object."); }
                        break;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="TypeEntity"></param>
        /// <param name="LogEntity"></param>
        /// <exception cref="Exception"></exception>
        public void AddListLog<T>(ADWebWindowsServicesTypeEntity TypeEntity, T LogEntity)
            where T : IEnumerable<IADWebWindowsServicesLogEntity>
        {
            using (ADWebWindowsServicesLogQuery ServicesQueryLog = new ADWebWindowsServicesLogQuery())
            {
                switch (TypeEntity)
                {
                    case ADWebWindowsServicesTypeEntity.Services:
                        if (LogEntity is IEnumerable<ADWebWindowsServicesLogEntity>)
                        {
                            ServicesQueryLog.SaveServicesLog(LogEntity);
                        }
                        else
                        {
                            throw new Exception("The log entity is not the IEnumerable<ADWebWindowsServicesLogEntity> class object."); }
                        break;
                    case ADWebWindowsServicesTypeEntity.Process:
                        if (LogEntity is IEnumerable<ADWebWindowsServicesProcessLogEntity>)
                        {
                            ServicesQueryLog.SaveServicesProcessLog(LogEntity);
                        }
                        else
                        {
                            throw new Exception("The log entity is not the IEnumerable<ADWebWindowsServicesProcessLogEntity> class object."); }
                        break;
                    case ADWebWindowsServicesTypeEntity.Task:
                        if (LogEntity is IEnumerable<ADWebWindowsServicesProcessLogEntity>)
                        {
                            ServicesQueryLog.SaveServicesProcessTaskLog(LogEntity);
                        }
                        else
                        { throw new Exception("The log entity is not the IEnumerable<ADWebWindowsServicesProcessTaskLogEntity> class object."); }
                        break;
                }
            }
        }
    }
}
