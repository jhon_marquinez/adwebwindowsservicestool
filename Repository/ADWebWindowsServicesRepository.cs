﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Query;
using ADWebWindowsServices.Entity;

namespace ADWebWindowsServices.Repository
{
    internal class ADWebWindowsServicesRepository : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// 
        /// </summary>
        private ADWebWindowsServicesQuery ServicesQuery = null;

        /// <summary>
        /// 
        /// </summary>
        public ADWebWindowsServicesRepository()
        {
            ServicesQuery = new ADWebWindowsServicesQuery();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public int GetServicesId(string serviceName)
        {
            return ServicesQuery.GetServicesId(serviceName);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public ADWebWindowsServicesEntity GetServices(string serviceName)
        {
            return ServicesQuery.GetServices(serviceName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesProcessEntity> GetProcessRunning(ADWebWindowsServicesEntity Services)
        {
            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessActive = null;
            ProcessActive = ServicesQuery.GetProcess(Services).Where(Proccess => Proccess.IsProcessRunning);
            return ProcessActive;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public IEnumerable<ADWebWindowsServicesProcessEntity> GetProcessNotRunning(ADWebWindowsServicesEntity Services)
        {
            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessDesactive = null;
            ProcessDesactive = ServicesQuery.GetProcess(Services).Where(Proccess => !Proccess.IsProcessRunning);
            return ProcessDesactive;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessTaskEntity> GetTaskActive(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesProcessTaskEntity> ServicesProcessActive = null;
            ServicesProcessActive = ServicesQuery.GetTask(Process).Where(Task => Task.ActiveTask);
            return ServicesProcessActive;
        }
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesProcessTaskEntity> GetTaskDesactive(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesProcessTaskEntity> ServicesProcessActive = null;
            ServicesProcessActive = ServicesQuery.GetTask(Process).Where(Task => !Task.ActiveTask);
            return ServicesProcessActive;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesConfigDataEntity> GetProcessConfigData(ADWebWindowsServicesProcessEntity Process)
        {
            return ServicesQuery.GetProcessConfigData(Process);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="keyConfig"></param>
        /// <returns></returns>
        public string GetProcessConfigData(ADWebWindowsServicesProcessEntity Process, string keyConfig)
        {
            return ServicesQuery.GetProcessConfigData(Process).FirstOrDefault(conf => conf.Key == keyConfig).Value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <returns></returns>
        public IEnumerable<ADWebWindowsServicesConfigDataEntity> GetTaskConfigData(ADWebWindowsServicesProcessTaskEntity Task)
        {
            return ServicesQuery.GetTaskConfigData(Task);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <param name="keyConfig"></param>
        /// <returns></returns>
        public string GetTaskConfigData(ADWebWindowsServicesProcessTaskEntity Task, string keyConfig)
        {
            return ServicesQuery.GetTaskConfigData(Task).FirstOrDefault(conf => conf.Key == keyConfig).Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <exception cref="Exception"></exception>
        public void StartProcess(ADWebWindowsServicesProcessEntity Process)
        {
            Process.InitialStart = Process.InitialStart ?? DateTime.Now;
            Process.LastStart = DateTime.Now;
            Process.IsProcessRunning = true;
            ServicesQuery.SaveStatusProcess(Process);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Task"></param>
        /// <exception cref="Exception"></exception>
        public void StartTask(ADWebWindowsServicesProcessTaskEntity Task)
        {
            Task.LastStart = DateTime.Now;
            ServicesQuery.SaveStatusTask(Task);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <exception cref="Exception"></exception>
        public void StopProcess(ADWebWindowsServicesProcessEntity Process)
        {
            Process.IsProcessRunning = false;
            ServicesQuery.SaveStatusProcess(Process);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="ScheduleTime"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool CheckIfProcessHasbeenExecuted(ADWebWindowsServicesProcessEntity Process, TimeSpan ScheduleTime, DateTime Date)
        {
            bool hasBeenEecuted = false;
            IEnumerable<ADWebWindowsServicesProcessExecutionLogEntity> ExecutionProcessLogs = ServicesQuery.GetExectionProcessLog(Process);
            ADWebWindowsServicesProcessExecutionLogEntity result =
                ExecutionProcessLogs.FirstOrDefault(executionLog => executionLog.ScheduleTime == ScheduleTime && executionLog.Date == Date);
            hasBeenEecuted = Convert.ToBoolean(result == null ? 0 : 1);
            return hasBeenEecuted;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Process"></param>
        /// <param name="ScheduleTime"></param>
        /// <param name="Date"></param>
        public void AddExecutionProcessLog(ADWebWindowsServicesProcessEntity Process, TimeSpan ScheduleTime, DateTime Date)
        {
            ServicesQuery.SaveExecutionProcessLog(new ADWebWindowsServicesProcessExecutionLogEntity(Process.IdProcess, ScheduleTime, Date));
        }
        /// <summary>
        /// 
        /// </summary>
        public bool CheckConnetivityWithDatabaseEngine()
        {
            return ServicesQuery.ThereIsConnectionWithDatabaseEngineActive();
        }
        /// <summary>
        /// 
        /// </summary>
        public void CreateGeneralServicesTables()
        {
            ServicesQuery.CreateGeneralServicesTables();
        }
        /// <summary>
        /// 
        /// </summary>
        public void DropGeneralServicesTables()
        {
            ServicesQuery.DropGeneralServicesTables();
        }
        /// <summary>
        /// 
        /// </summary>
        public bool ThereIsSomeServicesInstalled()
        {
            return ServicesQuery.ThereIsSomeServicesInstalled();
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebWindowsServicesRepository Destructor
        /// </summary>
        ~ADWebWindowsServicesRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// ADWebWindowsServicesRepository Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    ServicesQuery.Dispose();
                    ServicesQuery = null;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion
    }
}
