﻿
namespace ADWebWindowsServices.Resource
{
    public class ADWebWindowsServicesConstantValue
    {
        public static string DBConnectionNameAlfaDyser = "ADMSSQLS";
        public static string DBConnectionNameAlfaDyserWeb = "ADMySQL";
        public static char SeparatorScheduleTimeProcessExecution = ',';
        public static char SeparatorScheduleHoreInTimeProcessExecution = ':';
    }
}
