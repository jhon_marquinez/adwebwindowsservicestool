USE [G733_DATA]
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServices'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServices](
			[IdServices] [int] IDENTITY(1,1) PRIMARY KEY,
			[Name] [nvarchar](100) NOT NULL UNIQUE,
			[InstalledDate] [datetime] NOT NULL,
			[IntervalExecutionTimeMiliSecond] [int] NOT NULL
		)
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcess'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcess](
			[IdProcess] [int] IDENTITY(1,1) PRIMARY KEY,
			[IdServices] [int] NOT NULL,
			[Name] [nvarchar](100) NOT NULL UNIQUE,
			[InitialStart] [datetime] ,
			[LastStart] [datetime] ,
			[IsProcessRunning] [bit] NOT NULL,
			[ScheduleTime] [nvarchar](100) NULL 
			FOREIGN KEY ([IdServices]) 
				REFERENCES [imp].[ADWebWindowsServices]([IdServices])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		) 
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTask'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessTask](
			[IdTask] [int] IDENTITY(1,1) PRIMARY KEY,
			[IdProcess] [int] NOT NULL,
			[Name] [nvarchar](100) NOT NULL UNIQUE,
			[LastStart] [datetime] ,
			[ActiveTask] [bit] NOT NULL
			FOREIGN KEY ([IdProcess]) 
				REFERENCES [imp].[ADWebWindowsServicesProcess]([IdProcess])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)  
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessExecutionLog'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessExecutionLog](
			[IdExecutionLog] [int] IDENTITY(1,1) PRIMARY KEY,
			[IdProcess] [int] NOT NULL,
			[ScheduleTime] [time](3) NOT NULL,
			[Date] [date] NOT NULL,
			UNIQUE([IdProcess], [ScheduleTime],[Date]),
			FOREIGN KEY ([IdProcess])
				REFERENCES [imp].[ADWebWindowsServicesProcess]([IdProcess])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessConfigData'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessConfigData](
			[IdProcess] [int] ,
			[Key] [nvarchar](100) ,
			[Value] [nvarchar](100) NULL,
			PRIMARY KEY ([IdProcess],[Key]),
			FOREIGN KEY ([IdProcess]) 
				REFERENCES [imp].[ADWebWindowsServicesProcess]([IdProcess])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTaskConfigData'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessTaskConfigData](
			[IdTask] [int] ,
			[Key] [nvarchar](100) ,
			[Value] [nvarchar](100) NULL,
			PRIMARY KEY ([IdTask],[Key]),
			FOREIGN KEY ([IdTask]) 
				REFERENCES [imp].[ADWebWindowsServicesProcessTask]([IdTask])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesLog'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesLog](
			[IdLog] [int] IDENTITY(1,1) PRIMARY KEY,
			[IdServices] [int] NOT  NULL,
			[Status] [int] NOT NULL,
			[ExceptionJson] [nvarchar](max) NOT NULL,
			[TypeLog] [int] NOT NULL,
			[CreatedAt] [datetime] NOT NULL,
			FOREIGN KEY ([IdServices]) 
				REFERENCES [imp].[ADWebWindowsServices]([IdServices])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessLog'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessLog](
			[IdLog] [int] IDENTITY(1,1) PRIMARY KEY,
			[IdProcess] [int] NOT NULL,
			[TypeLog] [int] NOT NULL,
			[Message] [nvarchar](max) NOT NULL,
			[ExceptionJson] [nvarchar](max) NOT NULL,
			[CreatedAt] [datetime] NOT NULL,
			FOREIGN KEY ([IdProcess]) 
				REFERENCES [imp].[ADWebWindowsServicesProcess]([IdProcess])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)
	END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTaskLog'))
	BEGIN
		SET ANSI_NULLS ON
		SET QUOTED_IDENTIFIER ON
		CREATE TABLE [imp].[ADWebWindowsServicesProcessTaskLog](
			[IdLog] [int] IDENTITY(1,1) PRIMARY KEY,
			[IdTask] [int] NOT NULL,
			[TypeLog] [int] NOT NULL,
			[Message] [nvarchar](max) NOT NULL,
			[ExceptionJson] [nvarchar](max) NOT NULL,
			[CreatedAt] [datetime] NOT NULL,
			FOREIGN KEY ([IdTask]) 
				REFERENCES [imp].[ADWebWindowsServicesProcessTask]([IdTask])
					ON DELETE CASCADE 
					ON UPDATE CASCADE
		)
	END
GO