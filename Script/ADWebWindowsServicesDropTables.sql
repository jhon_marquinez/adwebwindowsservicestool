USE [G733_DATA]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesLog'))
	DROP TABLE [imp].[ADWebWindowsServicesLog]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessLog]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTaskLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessTaskLog]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTaskConfigData'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessTaskConfigData]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessConfigData'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessConfigData]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTask'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessTask]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessExecutionLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessExecutionLog]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcess'))
	DROP TABLE [imp].[ADWebWindowsServicesProcess]
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServices'))
	DROP TABLE [imp].[ADWebWindowsServices]
GO


